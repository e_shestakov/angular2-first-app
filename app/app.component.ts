import {Component} from '@angular/core';

@Component({
    selector: 'todo-app',
    templateUrl: './app/app.component.html',
    styleUrls: ['./app/app.component.css']
    //template : 'ddff 2'
})
export class AppComponent {
  title : string;
  todos : string[];

  constructor() {
    this.title = 'My angular title2';
    this.todos = [];
  }
  addTodo(input : HTMLInputElement) : void {
    console.log('add todo');
    this.todos.push(input.value);
    input.value = ''; 
    //console.log(input.value);
  }

}
